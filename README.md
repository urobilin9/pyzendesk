# PyZendesk

[![Travis CI Build Status](https://img.shields.io/travis/com/muflone/pyzendesk/master.svg)](https://www.travis-ci.com/github/muflone/pyzendesk)
[![CircleCI Build Status](https://img.shields.io/circleci/project/github/muflone/pyzendesk/master.svg)](https://circleci.com/gh/muflone/pyzendesk)

**Description:** API for Zendesk

**Copyright:** 2021 Fabio Castelli (Muflone) <muflone@muflone.com>

**License:** GPL-3+

**Source code:** https://github.com/muflone/pyzendesk

**Documentation:** http://www.muflone.com/pyzendesk/

# Description

PyZendesk is an attempt to build a common API to interact with any
[**Zendesk**](https://www.zendesk.com/) instance.

# System Requirements

* Python 3.x
* Python Requests 2.26.x (https://pypi.org/project/requests/)

# Usage

Please see the **samples** folder for some usage examples.
